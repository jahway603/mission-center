/* ui/performance_page/network.blp
 *
 * Copyright 2023 Romeo Calota
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Gtk 4.0;

template $PerformancePageNetwork: Box {
  orientation: vertical;

  WindowHandle {
    child: Box description {
      orientation: vertical;
      spacing: 7;
      hexpand: true;

      Box {
        spacing: 20;

        Label title_connection_type {
          styles [
            "title-1",
          ]

          hexpand: true;
          halign: start;
        }

        Label device_name {
          styles [
            "title-3",
          ]

          halign: end;
          ellipsize: middle;
        }
      }

      Box {
        Label {
          styles [
            "caption",
          ]

          hexpand: true;
          halign: start;
          label: _("Throughput");
        }

        Label max_y {
          styles [
            "caption",
          ]
        }
      }
    };
  }

  $GraphWidget usage_graph {
    vexpand: true;
    hexpand: true;

    height-request: 120;

    base-color: bind template.base-color;
    data-set-count: 2;
    scroll: true;
    auto-scale: true;
    auto-scale-pow2: true;
  }

  Box {
    margin-top: 2;
    margin-bottom: 10;

    Label graph_max_duration {
      styles [
        "caption",
      ]

      hexpand: true;
      halign: start;
      valign: start;
    }

    Label {
      styles [
        "caption",
      ]

      valign: start;
      label: "0";
    }
  }

  Box details {
    spacing: 20;

    visible: bind template.summary-mode inverted;

    Box dynamic_data {
      orientation: vertical;
      spacing: 10;

      Box {
        spacing: 5;

        Picture legend_send {
            can-shrink: false;
            content-fit: scale_down;
        }

        Box {
          orientation: vertical;
          spacing: 3;
          width-request: 100;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Send");
          }

          Label speed_send {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }
      }

      Box {
        spacing: 5;

        Picture legend_recv {
            can-shrink: false;
            content-fit: scale_down;
        }

        Box {
          orientation: vertical;
          spacing: 3;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Receive");
          }

          Label speed_recv {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }
      }
    }

    Box interface_info {
      spacing: 10;

      Box labels {
        orientation: vertical;
        spacing: 3;

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Interface name:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Connection type:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          visible: bind ssid.visible;
          label: _("SSID:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          visible: bind signal_strength.visible;
          label: _("Signal strength:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          visible: bind max_bitrate.visible;
          label: _("Maximum Bitrate:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          visible: bind frequency.visible;
          label: _("Frequency:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Hardware address:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("IPv4 address:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("IPv6 address:");
        }
      }

      Box values {
        orientation: vertical;
        spacing: 3;

        Label interface_name_label {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label connection_type_label {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label ssid {
          styles [
            "caption",
          ]

          halign: start;
          visible: false;
        }

        Image signal_strength {
          styles [
            "caption",
          ]

          halign: start;
          visible: false;
        }

        Label max_bitrate {
          styles [
            "caption",
          ]

          halign: start;
          visible: false;
        }

        Label frequency {
          styles [
            "caption",
          ]

          halign: start;
          visible: false;
        }

        Label hw_address {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label ipv4_address {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label ipv6_address {
          styles [
            "caption",
          ]

          halign: start;
        }
      }
    }
  }

  PopoverMenu context_menu {
    has-arrow: false;
    menu-model: context_menu_model;
  }
}

menu context_menu_model {
  section {
    item {
      label: _("Graph _Summary View");
      action: "graph.summary";
    }

    submenu {
      label: _("_View");

      item {
        label: _("CP_U");
        action: "graph.cpu";
      }

      item {
        label: _("_Memory");
        action: "graph.memory";
      }

      item {
        label: _("_Disk");
        action: "graph.disk";
      }

      item {
        label: _("_Network");
        action: "graph.network";
      }

      item {
        label: _("_GPU");
        action: "graph.gpu";
      }
    }

    item {
        label: _("Network Se_ttings");
        action: "graph.network-settings";
    }
  }

  section {
    item {
      label: _("_Copy");
      action: "graph.copy";
    }
  }
}
