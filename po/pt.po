# Portuguese translations for missioncenter.
# Copyright (C) 2023 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the missioncenter package.
# Rilson Joás, 2023.
# Rafael Fontenelle <rafaelff@gnome.org>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: missioncenter\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-20 08:11+0300\n"
"PO-Revision-Date: 2023-11-15 06:05+0000\n"
"Last-Translator: Tiago Lucas Flach <tiagolucas9830@gmail.com>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/"
"mission-center/mission-center/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.2-dev\n"

#: data/io.missioncenter.MissionCenter.desktop.in:3
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:4
msgid "Mission Center"
msgstr "Mission Center"

#: data/io.missioncenter.MissionCenter.desktop.in:10
msgid ""
"Task manager;Resource monitor;System monitor;Processor;Processes;Performance "
"monitor;CPU;GPU;Disc;Disk;Memory;Network;Utilisation;Utilization"
msgstr ""
"Gestor de tarefas;Monitor de recursos;Monitor do sistema;Processador;"
"Processos;Monitor de desempenho;CPU;GPU;Disco;Disco;Memória;Rede;Uso;"
"Utilização"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:5
msgid "Mission Center Developers"
msgstr "Desenvolvedores do Mission Center"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:14
msgid "Monitor system resource usage"
msgstr "Monitorar o uso de recursos do sistema"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:16
msgid "Monitor your CPU, Memory, Disk, Network and GPU usage"
msgstr "Monitore o uso de CPU, memória, disco, rede e GPU"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:17
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:120
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:162
msgid "Features:"
msgstr "Características:"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:19
msgid "Monitor overall or per-thread CPU usage"
msgstr "Monitore o uso geral ou por thread da CPU"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:20
msgid ""
"See system process, thread, and handle count, uptime, clock speed (base and "
"current), cache sizes"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:21
msgid "Monitor RAM and Swap usage"
msgstr "Monitorizar utilização de RAM e Swap"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:22
msgid "See a breakdown how the memory is being used by the system"
msgstr "Veja um detalhamento de como a memória está sendo usada pelo sistema"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:23
msgid "Monitor Disk utilization and transfer rates"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:24
msgid "Monitor network utilization and transfer speeds"
msgstr "Monitorizar utilização da rede e velocidades de transferência"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:25
msgid ""
"See network interface information such as network card name, connection type "
"(Wi-Fi or Ethernet), wireless speeds and frequency, hardware address, IP "
"address"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:28
msgid ""
"Monitor overall GPU usage, video encoder and decoder usage, memory usage and "
"power consumption, powered by the popular NVTOP project"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:31
msgid "See a breakdown of resource usage by app and process"
msgstr "Veja um detalhamento do uso de recursos por aplicativo e processo"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:32
msgid "Supports a minified summary view for simple monitoring"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:33
msgid ""
"Use OpenGL rendering for all the graphs in an effort to reduce CPU and "
"overall resource usage"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:34
msgid "Uses GTK4 and Libadwaita"
msgstr "Utiliza GTK4 e Libadwaita"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:35
msgid "Written in Rust"
msgstr "Desenvolvido em Rust"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:36
msgid "Flatpak first"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:38
msgid "Limitations (there is ongoing work to overcome all of these):"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:40
msgid "Disk utilization percentage might not be accurate"
msgstr ""
"Percentagens apresentadas na utilização de Disco podem não ser precisas"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:41
msgid "No per-process network usage"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:42
msgid "No per-process GPU usage"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:43
msgid ""
"GPU support is experimental and only AMD and nVidia GPUs can be monitored"
msgstr ""
"Suporte de GPU é experimental, apenas podem ser monitorizados GPUs da AMD e "
"nVidia"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:45
msgid "Comments, suggestions, bug reports and contributions welcome"
msgstr "Comentários, sugestões, reporte de bugs e contribuições são bem-vindos"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:122
msgid ""
"Add an option, to the context menu, to show kernel times, in the CPU graphs"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:123
msgid "Add CPU temperature, where possible"
msgstr "Adicione a temperatura da CPU, quando possível"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:124
msgid ""
"Implement smarter rounding and display precision of values throughout the "
"app by QwertyChouskie"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:125
msgid ""
"Use metainfo instead of appdata for application metadata by David Guglielmi"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:127
msgid "Fixes:"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:129
msgid ""
"Fixed a regression where natively installed browsers were not showing up in "
"the apps list on Fedora"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:130
msgid ""
"Fixed a regression where Flatpak apps were not showing up in the apps list "
"on ArchLinux Plasma"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:131
msgid ""
"Fixed a bug where the Maps app desktop file wasn't being parsed correctly"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:132
msgid ""
"Moved some performance related logs to g_debug to prevent spamming of system "
"logs"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:134
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:177
msgid "Translations:"
msgstr "Traduções:"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:136
msgid "New translation to Korean by Seong-ho Cho"
msgstr "Nova tradução para o coreano por Seong-ho Cho"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:137
msgid "New translation to Dutch by Gert"
msgstr "Nova tradução para holandês por Gert"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:138
msgid "New translation to Polish by _Ghost_"
msgstr "Nova tradução para polonês por _Ghost_"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:139
msgid "New translation to Italian by beppeilgommista"
msgstr "Nova tradução para o italiano por beppeilgommista"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:140
msgid "New translation to Portuguese (Brazil) by Gérson da Fonseca Henzel"
msgstr "Nova tradução para o português (Brasil) de Gérson da Fonseca Henzel"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:141
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:186
msgid "Updated Spanish translation"
msgstr "Tradução atualizada em espanhol"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:142
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:145
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:190
msgid "Updated Finnish translation"
msgstr "Tradução finlandesa atualizada"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:143
msgid "Updated Russian translation"
msgstr "Tradução russa atualizada"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:144
msgid "Updated Chinese (Simplified) translation"
msgstr "Tradução atualizada do chinês (simplificado)"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:146
msgid "Updated Greek translation"
msgstr "Tradução grega atualizada"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:147
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:189
msgid "Updated German translation"
msgstr "Tradução alemã atualizada"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:155
msgid ""
"Hotfix release to fix a bug that caused the app to not display any "
"information for some users"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:164
msgid "New app icon by QwertyChouskie"
msgstr "Novo ícone do aplicativo por QwertyChouskie"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:165
msgid "Add ability to stop and force stop apps and processes"
msgstr ""
"Adicionada capacidade de interromper e forçar a interrupção de aplicativos e "
"processos"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:166
msgid ""
"Running apps are now shown more reliably, and should reflect most if not all "
"running apps"
msgstr ""
"Os aplicativos em execução agora são mostrados de forma mais confiável e "
"devem refletir a maioria, se não todos, os aplicativos em execução"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:167
msgid "Experimental support for Snap apps in the running apps list"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:168
msgid ""
"Added a setting to show resource consumption individually per process or "
"cumulated with their descendants"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:171
msgid ""
"Added a setting to enable persistent sorting in the apps and processes list"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:172
msgid ""
"Data gathering is now more versatile and will permit new features to be "
"added quicker and easier"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:175
msgid "App can now be built from GNOME Builder"
msgstr "O aplicativo agora pode ser criado a partir do GNOME Builder"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:179
msgid "New translation to Norwegian Bokmål by Allan Nordhøy"
msgstr "Nova tradução para o norueguês Bokmål por Allan Nordhøy"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:180
msgid "New translation to Russian by Ivan Maslikhov"
msgstr "Nova tradução para o russo por Ivan Maslikhov"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:181
msgid "New translation to Slovak by mthw0"
msgstr "Nova tradução para eslovaco por mthw0"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:182
msgid "New translation to Greek by Yiannis Ioannides"
msgstr "Nova tradução para o grego por Yiannis Ioannides"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:183
msgid "New translation to Chinese (Simplified) by foxer NS"
msgstr "Nova tradução para chinês (simplificado) por foxer NS"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:184
msgid "New translation to French by Link Mauve"
msgstr "Nova tradução para o francês por Link Mauve"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:185
msgid "New translation to Hungarian by Kovács Bálint Hunor"
msgstr "Nova tradução para o húngaro por Kovács Bálint Hunor"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:187
msgid "Updated Czech translation"
msgstr "Tradução tcheca atualizada"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:188
msgid "Updated Portuguese translation"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:191
msgid "Fixes for Chinese translations by foxer NS"
msgstr "Correções para traduções chinesas por foxer NS"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:199
msgid "Add Spanish translation by Óscar Fernández Díaz"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:200
msgid ""
"The memory tab now shows configured memory speed instead of the maximum "
"supported by the modules"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:203
msgid "Add German translation by TecCheck"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:204
msgid ""
"When a process uses large amounts of CPU or RAM it is now highlighted in the "
"Apps and Processes list"
msgstr ""
"Quando um processo utiliza grandes quantidades de CPU ou RAM, o mesmo agora "
"é apresentado destacado nas listas dos Processos e Aplicações"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:207
msgid "Add initial support for building for ARM64"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:208
msgid ""
"Take into account multiple CPU cores and cache sharing when calculating "
"cache sizes"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:209
msgid ""
"Fix browsers, installed as native packages, not showing up in the Apps list"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:210
msgid "Translation updates for Traditional Chinese by Peter Dave Hello"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:218
msgid "Translation fixes for Portuguese by Rafael Fontenelle"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:219
msgid ""
"Only show a link-local IPv6 address if no other IPv6 exists by Maximilian"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:220
#, fuzzy
msgid "Add Traditional Chinese locale by Peter Dave Hello"
msgstr "Adicionado chinês tradicional por Peter Dave Hello"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:221
msgid "Add category for application menu by Renner0E"
msgstr "Adicionar categoria ao menu do aplicativo Renner0E"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:222
msgid ""
"Fix a parsing error when parsing the output of `dmidecode` that lead to a "
"panic"
msgstr ""
"Corrigido um erro de análise ao analisar a saída de `dmidecode` que causava "
"pânico"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:223
msgid ""
"Use a fallback if `/sys/devices/system/cpu/cpu0/cpufreq/base_frequency` does "
"not exist, when getting CPU base speed information"
msgstr ""
"Use um fallback se `/sys/devices/system/cpu/cpu0/cpufreq/base_frequency` não "
"existir, ao obter informações de velocidade básica da CPU"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:226
msgid "Update GPU tab UI to be more adaptive for smaller resolutions"
msgstr ""
"Atualizada a interface da guia GPU para ser mais adaptável para resoluções "
"menores"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:234
msgid "Added Czech translation by ondra05"
msgstr "Adicionada tradução em tcheco por ondra05"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:235
msgid "Added Portuguese translation by Rilson Joás"
msgstr "Adicionada tradução para o português de Rilson Joás"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:236
msgid ""
"Add keywords to desktop file to improve search function of desktop "
"environments by Hannes Kuchelmeister"
msgstr ""
"Adicionadas palavras-chave ao arquivo da área de trabalho para melhorar a "
"função de pesquisa de ambientes de área de trabalho por Hannes Kuchelmeister"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:239
msgid "Fixed a bug where the app and process list was empty for some users"
msgstr ""
"Corrigido um bug onde a lista de aplicativos e processos estava vazia para "
"alguns usuários"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:247
msgid "Fix a crash that occurs when the system is under heavy load"
msgstr "Corrija uma falha que ocorre quando o sistema está sob carga pesada"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:255
msgid "First official release!"
msgstr "Primeiro lançamento oficial!"

#: data/io.missioncenter.MissionCenter.gschema.xml:29
msgid "Which page is shown on application startup"
msgstr "Qual página deve ser exibida na inicialização do aplicativo"

#: data/io.missioncenter.MissionCenter.gschema.xml:35
msgid "How fast should the data be refreshed and the UI updated"
msgstr ""
"Com que velocidade os dados devem ser atualizados e a interface do usuário "
"deve ser atualizada"

#: data/io.missioncenter.MissionCenter.gschema.xml:40
#: resources/ui/preferences/page.blp:38
msgid "Parent and child process stats are shown individually or merged upwards"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:45
msgid "Column sorting is persisted across app restarts"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:50
msgid "The column id by which the Apps page view is sorted"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:55
msgid "The sorting direction of the Apps page view"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:61
msgid "Which graph is shown on the CPU performance page"
msgstr "Qual gráfico é mostrado na página de desempenho da CPU"

#: data/io.missioncenter.MissionCenter.gschema.xml:66
msgid "Show kernel times in the CPU graphs"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:71
msgid "Which page is shown on application startup, in the performance tab"
msgstr ""
"Qual página é mostrada na inicialização do aplicativo, na aba de Desempenho"

#: resources/ui/performance_page/cpu.blp:42 src/apps_page/mod.rs:1049
#: src/performance_page/mod.rs:264
msgid "CPU"
msgstr "CPU"

#: resources/ui/performance_page/cpu.blp:74
#: resources/ui/performance_page/cpu.blp:152
#: resources/ui/performance_page/gpu.blp:227
msgid "Utilization"
msgstr "Utilização"

#: resources/ui/performance_page/cpu.blp:84
msgid "100%"
msgstr "100%"

#: resources/ui/performance_page/cpu.blp:174
msgid "Speed"
msgstr "Velocidade"

#: resources/ui/performance_page/cpu.blp:201 src/apps_page/mod.rs:454
msgid "Processes"
msgstr "Processos"

#: resources/ui/performance_page/cpu.blp:223
msgid "Threads"
msgstr "Threads"

#: resources/ui/performance_page/cpu.blp:245
msgid "Handles"
msgstr "Identificadores"

#: resources/ui/performance_page/cpu.blp:268
msgid "Up time"
msgstr "Tempo de atividade"

#: resources/ui/performance_page/cpu.blp:294
msgid "Base Speed:"
msgstr "Velocidade base:"

#: resources/ui/performance_page/cpu.blp:303
msgid "Sockets:"
msgstr "Sockets:"

#: resources/ui/performance_page/cpu.blp:312
msgid "Virtual processors:"
msgstr "Processadores virtuais:"

#: resources/ui/performance_page/cpu.blp:321
msgid "Virtualization:"
msgstr "Virtualização:"

#: resources/ui/performance_page/cpu.blp:330
msgid "Virtual machine:"
msgstr "Máquina virtual:"

#: resources/ui/performance_page/cpu.blp:339
msgid "L1 cache:"
msgstr "Cache L1:"

#: resources/ui/performance_page/cpu.blp:348
msgid "L2 cache:"
msgstr "Cache L2:"

#: resources/ui/performance_page/cpu.blp:357
msgid "L3 cache:"
msgstr "Cache L3:"

#: resources/ui/performance_page/cpu.blp:442
msgid "Change G_raph To"
msgstr "Mudar g_ráfico para"

#: resources/ui/performance_page/cpu.blp:445
msgid "Overall U_tilization"
msgstr "U_tilização geral"

#: resources/ui/performance_page/cpu.blp:450
msgid "Logical _Processors"
msgstr "_Processadores lógicos"

#: resources/ui/performance_page/cpu.blp:456
msgid "Show Kernel Times"
msgstr ""

#: resources/ui/performance_page/cpu.blp:463
#: resources/ui/performance_page/disk.blp:386
#: resources/ui/performance_page/gpu.blp:549
#: resources/ui/performance_page/memory.blp:402
#: resources/ui/performance_page/network.blp:369
msgid "Graph _Summary View"
msgstr "Visão re_sumida"

#: resources/ui/performance_page/cpu.blp:468
#: resources/ui/performance_page/disk.blp:391
#: resources/ui/performance_page/gpu.blp:554
#: resources/ui/performance_page/memory.blp:407
#: resources/ui/performance_page/network.blp:374
msgid "_View"
msgstr "_Visão"

#: resources/ui/performance_page/cpu.blp:471
#: resources/ui/performance_page/disk.blp:394
#: resources/ui/performance_page/gpu.blp:557
#: resources/ui/performance_page/memory.blp:410
#: resources/ui/performance_page/network.blp:377
msgid "CP_U"
msgstr "CP_U"

#: resources/ui/performance_page/cpu.blp:476
#: resources/ui/performance_page/disk.blp:399
#: resources/ui/performance_page/gpu.blp:562
#: resources/ui/performance_page/memory.blp:415
#: resources/ui/performance_page/network.blp:382
msgid "_Memory"
msgstr "_Memória"

#: resources/ui/performance_page/cpu.blp:481
#: resources/ui/performance_page/disk.blp:404
#: resources/ui/performance_page/gpu.blp:567
#: resources/ui/performance_page/memory.blp:420
#: resources/ui/performance_page/network.blp:387
msgid "_Disk"
msgstr "_Disco"

#: resources/ui/performance_page/cpu.blp:486
#: resources/ui/performance_page/disk.blp:409
#: resources/ui/performance_page/gpu.blp:572
#: resources/ui/performance_page/memory.blp:425
#: resources/ui/performance_page/network.blp:392
msgid "_Network"
msgstr "R_ede"

#: resources/ui/performance_page/cpu.blp:491
#: resources/ui/performance_page/disk.blp:414
#: resources/ui/performance_page/gpu.blp:577
#: resources/ui/performance_page/memory.blp:430
#: resources/ui/performance_page/network.blp:397
msgid "_GPU"
msgstr "_GPU"

#: resources/ui/performance_page/cpu.blp:499
#: resources/ui/performance_page/disk.blp:422
#: resources/ui/performance_page/gpu.blp:585
#: resources/ui/performance_page/memory.blp:438
#: resources/ui/performance_page/network.blp:410
msgid "_Copy"
msgstr "_Cópia"

#: resources/ui/performance_page/disk.blp:64
#: resources/ui/performance_page/disk.blp:188
msgid "Active time"
msgstr "Tempo de atividade"

#: resources/ui/performance_page/disk.blp:122
msgid "Disk transfer rate"
msgstr "Taxa de transferência de disco"

#: resources/ui/performance_page/disk.blp:211
msgid "Average response time"
msgstr "Tempo médio de resposta"

#: resources/ui/performance_page/disk.blp:247
msgid "Read speed"
msgstr "Velocidade de leitura"

#: resources/ui/performance_page/disk.blp:279
msgid "Write speed"
msgstr "Velocidade de escrita"

#: resources/ui/performance_page/disk.blp:307
msgid "Capacity:"
msgstr "Capacidade:"

#: resources/ui/performance_page/disk.blp:316
msgid "Formatted:"
msgstr "Formatado:"

#: resources/ui/performance_page/disk.blp:325
msgid "System disk:"
msgstr "Disco do sistema:"

#: resources/ui/performance_page/disk.blp:334
#: resources/ui/performance_page/memory.blp:345
msgid "Type:"
msgstr "Tipo:"

#: resources/ui/performance_page/gpu.blp:71
msgid "Overall utilization"
msgstr "Utilização geral"

#: resources/ui/performance_page/gpu.blp:110
msgid "Video encode"
msgstr "Codificação de vídeo"

#: resources/ui/performance_page/gpu.blp:143
msgid "Video decode"
msgstr "Decodificação de vídeo"

#: resources/ui/performance_page/gpu.blp:180
#: resources/ui/performance_page/gpu.blp:350
#: resources/ui/performance_page/memory.blp:73
msgid "Memory usage"
msgstr "Uso de memória"

#: resources/ui/performance_page/gpu.blp:250
msgid "Clock Speed"
msgstr "Velocidade do clock"

#: resources/ui/performance_page/gpu.blp:295
msgid "Power draw"
msgstr "Uso de poder"

#: resources/ui/performance_page/gpu.blp:395
msgid "Memory speed"
msgstr "Velocidade de memória"

#: resources/ui/performance_page/gpu.blp:440
msgid "Temperature"
msgstr "Temperatura"

#: resources/ui/performance_page/gpu.blp:470
msgid "OpenGL version:"
msgstr "Versão do OpenGL:"

#: resources/ui/performance_page/gpu.blp:479
msgid "Vulkan version:"
msgstr "Versão do Vulkan:"

#: resources/ui/performance_page/gpu.blp:488
msgid "PCI Express speed:"
msgstr "Velocidade de PCI Express:"

#: resources/ui/performance_page/gpu.blp:497
msgid "PCI bus address:"
msgstr "Endereço do PCI bus:"

#: resources/ui/performance_page/memory.blp:28
msgid "Some information requires administrative privileges"
msgstr "Algumas informações precisam de privilégios administrativos"

#: resources/ui/performance_page/memory.blp:29
msgid "_Authenticate"
msgstr "_Autenticar"

#: resources/ui/performance_page/memory.blp:52 src/apps_page/mod.rs:1055
#: src/performance_page/mod.rs:319
msgid "Memory"
msgstr "Memória"

#: resources/ui/performance_page/memory.blp:130
msgid "Memory composition"
msgstr "Composição da memória"

#: resources/ui/performance_page/memory.blp:166
msgid "In use"
msgstr "Em uso"

#: resources/ui/performance_page/memory.blp:189
msgid "Available"
msgstr "Disponível"

#: resources/ui/performance_page/memory.blp:217
msgid "Committed"
msgstr "Comprometido"

#: resources/ui/performance_page/memory.blp:240
msgid "Cached"
msgstr "Em cache"

#: resources/ui/performance_page/memory.blp:267
msgid "Swap available"
msgstr "Swap disponível"

#: resources/ui/performance_page/memory.blp:290
msgid "Swap used"
msgstr "Swap em uso"

#: resources/ui/performance_page/memory.blp:318
msgid "Speed:"
msgstr "Velocidade:"

#: resources/ui/performance_page/memory.blp:327
msgid "Slots used:"
msgstr "Slots usados:"

#: resources/ui/performance_page/memory.blp:336
msgid "Form factor:"
msgstr "Formato de memória:"

#: resources/ui/performance_page/network.blp:62
msgid "Throughput"
msgstr "Taxa de transferência"

#: resources/ui/performance_page/network.blp:139
msgid "Send"
msgstr "Enviado"

#: resources/ui/performance_page/network.blp:170
msgid "Receive"
msgstr "Recebido"

#: resources/ui/performance_page/network.blp:197
msgid "Interface name:"
msgstr "Nome do adaptador:"

#: resources/ui/performance_page/network.blp:206
msgid "Connection type:"
msgstr "Tipo de conexão:"

#: resources/ui/performance_page/network.blp:216
msgid "SSID:"
msgstr "SSID:"

#: resources/ui/performance_page/network.blp:226
msgid "Signal strength:"
msgstr "Força do sinal:"

#: resources/ui/performance_page/network.blp:236
msgid "Maximum Bitrate:"
msgstr "Fluxo máximo:"

#: resources/ui/performance_page/network.blp:246
msgid "Frequency:"
msgstr "Frequência:"

#: resources/ui/performance_page/network.blp:255
msgid "Hardware address:"
msgstr "Endereço local:"

#: resources/ui/performance_page/network.blp:264
msgid "IPv4 address:"
msgstr "Endereço IPv4:"

#: resources/ui/performance_page/network.blp:273
msgid "IPv6 address:"
msgstr "Endereço IPv6:"

#: resources/ui/performance_page/network.blp:403
msgid "Network Se_ttings"
msgstr "C_onfigurações de rede"

#: resources/ui/preferences/page.blp:6
msgid "General Settings"
msgstr "Configurações gerais"

#: resources/ui/preferences/page.blp:9
msgid "Update Speed"
msgstr "Velocidade de atualização"

#: resources/ui/preferences/page.blp:12 src/preferences/page.rs:113
#: src/preferences/page.rs:262
msgid "Fast"
msgstr "Rápida"

#: resources/ui/preferences/page.blp:13
msgid "Refresh every half second"
msgstr "Atualizar a cada meio segundo"

#: resources/ui/preferences/page.blp:17 src/preferences/page.rs:109
#: src/preferences/page.rs:122 src/preferences/page.rs:266
#: src/preferences/page.rs:282
msgid "Normal"
msgstr "Normal"

#: resources/ui/preferences/page.blp:18
msgid "Refresh every second"
msgstr "Atualizar a cada segundo"

#: resources/ui/preferences/page.blp:22 src/preferences/page.rs:105
#: src/preferences/page.rs:270
msgid "Slow"
msgstr "Lenta"

#: resources/ui/preferences/page.blp:23
msgid "Refresh every second and a half"
msgstr "Atualizar a cada segundo e meio"

#: resources/ui/preferences/page.blp:27 src/preferences/page.rs:101
#: src/preferences/page.rs:274
msgid "Very Slow"
msgstr "Muito lenta"

#: resources/ui/preferences/page.blp:28
msgid "Refresh every 2 seconds"
msgstr "Atualizar a cada 2 segundos"

#: resources/ui/preferences/page.blp:34
#, fuzzy
msgid "App Page Settings"
msgstr "Configurações gerais"

#: resources/ui/preferences/page.blp:37
msgid "Merge Process Stats"
msgstr ""

#: resources/ui/preferences/page.blp:42
msgid "Remember Sorting"
msgstr ""

#: resources/ui/preferences/page.blp:43
msgid "Remember the sorting of the app and process list across app restarts"
msgstr ""

#: resources/ui/window.blp:48
msgid "Type a name or PID to search"
msgstr "Digite um nome ou identificador para iniciar busca"

#: resources/ui/window.blp:90
msgid "Loading..."
msgstr "A carregar..."

#: resources/ui/window.blp:104
msgid "Performance"
msgstr "Desempenho"

#: resources/ui/window.blp:113 src/apps_page/mod.rs:447
msgid "Apps"
msgstr "Aplicações"

#: resources/ui/window.blp:124
msgid "_Preferences"
msgstr "_Preferências"

#: resources/ui/window.blp:129
msgid "_About MissionCenter"
msgstr "_Sobre o Mission Center"

#: src/apps_page/mod.rs:1037
msgid "Name"
msgstr "Nome"

#: src/apps_page/mod.rs:1043
msgid "PID"
msgstr "PID"

#: src/apps_page/mod.rs:1061
msgid "Disk"
msgstr "Disco"

#. ContentType::App
#: src/apps_page/list_item.rs:315
msgid "Stop Application"
msgstr "Parar aplicação"

#: src/apps_page/list_item.rs:315
msgid "Force Stop Application"
msgstr "Forçar o término da aplicação"

#. ContentType::Process
#: src/apps_page/list_item.rs:319
msgid "Stop Process"
msgstr "Parar processo"

#: src/apps_page/list_item.rs:319
msgid "Force Stop Process"
msgstr "Forçar o término do processo"

#: src/performance_page/widgets/mem_composition_widget.rs:224
msgid ""
"In use ({}B)\n"
"\n"
"Memory used by the operating system and running applications"
msgstr ""
"Em utilização ({}iB)\n"
"\n"
"Memória utilizada pelo sistema operativo e pelas aplicações"

#: src/performance_page/widgets/mem_composition_widget.rs:236
msgid ""
"Modified ({}B)\n"
"\n"
"Memory whose contents must be written to disk before it can be used by "
"another process"
msgstr ""
"Modificado ({}B)\n"
"\n"
"Memória cujo conteúdo tem de ser gravado no disco antes de poder ser "
"utilizado por outro processo"

#: src/performance_page/widgets/mem_composition_widget.rs:257
#, fuzzy
msgid ""
"Standby ({}B)\n"
"\n"
"Memory that contains cached data and code that is not actively in use"
msgstr ""
"Em espera ({}iB)\n"
"\n"
"Memória que contém dados em cache e código ainda não ativo"

#: src/performance_page/widgets/mem_composition_widget.rs:266
#, fuzzy
msgid ""
"Free ({}B)\n"
"\n"
"Memory that is not currently in use, and that will be repurposed first when "
"the operating system, drivers, or applications need more memory"
msgstr ""
"Livre ({}iB)\n"
"\n"
"Memória que não se encontra em uso, e que vai ser ter prioridade quando o "
"sistema, os drivers ou aplicativos precisem de mais memória"

#: src/performance_page/mod.rs:372 src/performance_page/disk.rs:195
msgid "Disk {} ({})"
msgstr "Disco {} ({})"

#: src/performance_page/mod.rs:376
msgid "HDD"
msgstr "HDD"

#: src/performance_page/mod.rs:377
msgid "SSD"
msgstr "SSD"

#: src/performance_page/mod.rs:378
msgid "NVMe"
msgstr "NVMe"

#: src/performance_page/mod.rs:379
msgid "eMMC"
msgstr "eMMC"

#: src/performance_page/mod.rs:380
msgid "iSCSI"
msgstr "iSCSI"

#: src/performance_page/mod.rs:381 src/performance_page/cpu.rs:310
#: src/performance_page/cpu.rs:323 src/performance_page/cpu.rs:333
#: src/performance_page/cpu.rs:339 src/performance_page/gpu.rs:255
#: src/performance_page/memory.rs:338 src/performance_page/network.rs:375
#: src/performance_page/network.rs:396 src/performance_page/network.rs:405
#: src/performance_page/network.rs:442 src/performance_page/network.rs:520
msgid "Unknown"
msgstr "Desconhecido"

#: src/performance_page/mod.rs:451 src/performance_page/network.rs:331
msgid "Ethernet"
msgstr "Ethernet"

#: src/performance_page/mod.rs:452 src/performance_page/network.rs:338
msgid "Wi-Fi"
msgstr "Wi-Fi"

#: src/performance_page/mod.rs:453 src/performance_page/network.rs:340
msgid "Other"
msgstr "Outro"

#: src/performance_page/mod.rs:541
msgid "GPU {}"
msgstr "GPU {}"

#: src/performance_page/mod.rs:702 src/performance_page/mod.rs:710
#, fuzzy
msgid "{}: {} {}bps"
msgstr "{} {}bps"

#: src/performance_page/cpu.rs:318
msgid "Supported"
msgstr "Compatível"

#: src/performance_page/cpu.rs:320 src/performance_page/gpu.rs:266
msgid "Unsupported"
msgstr "Não compatível"

#: src/performance_page/cpu.rs:328 src/performance_page/disk.rs:223
msgid "Yes"
msgstr "Sim"

#: src/performance_page/cpu.rs:330 src/performance_page/disk.rs:225
msgid "No"
msgstr "Não"

#: src/performance_page/cpu.rs:698
#, fuzzy
msgid "Utilization over {} seconds"
msgstr "% de uso em {} segundos"

#: src/performance_page/cpu.rs:702 src/performance_page/disk.rs:380
#: src/performance_page/memory.rs:401 src/performance_page/network.rs:606
msgid "{} seconds"
msgstr "{} segundos"

#: src/performance_page/disk.rs:251
msgid "{} {}{}B/s"
msgstr "{} {}{}B/s"

#: src/performance_page/network.rs:416 src/performance_page/network.rs:422
#: src/performance_page/network.rs:430
msgid "{} {}bps"
msgstr "{} {}bps"

#: src/performance_page/network.rs:455 src/performance_page/network.rs:471
msgid "N/A"
msgstr "N/A"

#: src/application.rs:261
msgid "translator-credits"
msgstr "Rafael Fontenelle <rafaelff@gnome.org>"

#~ msgid ""
#~ "The application currently only supports monitoring, you cannot stop "
#~ "processes for example"
#~ msgstr ""
#~ "De momento a aplicação apenas suporta monitorização, não é possível parar "
#~ "processos por exemplo"

#~ msgid "% Utilization"
#~ msgstr "% de utilização"

#~ msgid "{}: {} {}bps {}: {} {}bps"
#~ msgstr "{}: {} {}bps {}: {} {}bps"

#~ msgid "_Quit"
#~ msgstr "_Sair"

#~ msgid "No description"
#~ msgstr "Sem descrição"
