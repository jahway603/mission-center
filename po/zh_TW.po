# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the missioncenter package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: missioncenter\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-20 08:11+0300\n"
"PO-Revision-Date: 2023-08-07 03:54+0000\n"
"Last-Translator: NF <muwuren@gmail.com>\n"
"Language-Team: Chinese (Traditional) <https://hosted.weblate.org/projects/"
"mission-center/mission-center/zh_Hant/>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.0-dev\n"

#: data/io.missioncenter.MissionCenter.desktop.in:3
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:4
msgid "Mission Center"
msgstr "任務中心"

#: data/io.missioncenter.MissionCenter.desktop.in:10
msgid ""
"Task manager;Resource monitor;System monitor;Processor;Processes;Performance "
"monitor;CPU;GPU;Disc;Disk;Memory;Network;Utilisation;Utilization"
msgstr ""
"工作管理員;資源監控器;系統監控器;處理器;處理程序;效能監控器;CPU;GPU;磁碟;磁"
"碟;記憶體;網路;使用率;使用率"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:5
msgid "Mission Center Developers"
msgstr "任務中心開發者"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:14
msgid "Monitor system resource usage"
msgstr "監控系統資源使用情況"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:16
msgid "Monitor your CPU, Memory, Disk, Network and GPU usage"
msgstr "監控您的 CPU、記憶體、磁碟、網路和 GPU 使用情況"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:17
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:120
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:162
msgid "Features:"
msgstr "功能："

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:19
msgid "Monitor overall or per-thread CPU usage"
msgstr "監控整體或每個執行緒的 CPU 使用情況"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:20
msgid ""
"See system process, thread, and handle count, uptime, clock speed (base and "
"current), cache sizes"
msgstr ""
"檢視系統處理程序、執行緒和控制代碼數量、運作時間、時脈（基礎及目前）、快取大"
"小"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:21
msgid "Monitor RAM and Swap usage"
msgstr "監控 RAM 和 Swap 使用情況"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:22
msgid "See a breakdown how the memory is being used by the system"
msgstr "檢視系統如何使用記憶體的詳細資訊"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:23
msgid "Monitor Disk utilization and transfer rates"
msgstr "監控磁碟使用情況和傳輸速率"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:24
msgid "Monitor network utilization and transfer speeds"
msgstr "監控網路使用情況和傳輸速度"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:25
msgid ""
"See network interface information such as network card name, connection type "
"(Wi-Fi or Ethernet), wireless speeds and frequency, hardware address, IP "
"address"
msgstr ""
"檢視網路介面資訊，如網路卡名稱、連線類型（Wi-Fi 或 Ethernet）、無線速度和頻"
"率、硬體位址、IP 位址"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:28
msgid ""
"Monitor overall GPU usage, video encoder and decoder usage, memory usage and "
"power consumption, powered by the popular NVTOP project"
msgstr ""
"監控整體 GPU 使用情況、視訊編碼器和解碼器使用情況、記憶體使用情況和功耗，由熱"
"門的 NVTOP 專案提供支援"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:31
msgid "See a breakdown of resource usage by app and process"
msgstr "檢視應用程式和處理程序的資源使用詳細資訊"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:32
msgid "Supports a minified summary view for simple monitoring"
msgstr "支援縮小的摘要檢視以進行簡單監控"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:33
msgid ""
"Use OpenGL rendering for all the graphs in an effort to reduce CPU and "
"overall resource usage"
msgstr "使用 OpenGL 渲染所有圖表，以盡可能減少 CPU 和整體資源使用"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:34
msgid "Uses GTK4 and Libadwaita"
msgstr "使用 GTK4 和 Libadwaita"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:35
msgid "Written in Rust"
msgstr "使用 Rust 撰寫"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:36
msgid "Flatpak first"
msgstr "Flatpak 優先"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:38
msgid "Limitations (there is ongoing work to overcome all of these):"
msgstr "限制（目前正在努力克服這些限制）："

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:40
msgid "Disk utilization percentage might not be accurate"
msgstr "磁碟使用率可能不準確"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:41
msgid "No per-process network usage"
msgstr "沒有每個處理程序的網路使用情況"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:42
msgid "No per-process GPU usage"
msgstr "沒有每個處理程序的 GPU 使用情況"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:43
msgid ""
"GPU support is experimental and only AMD and nVidia GPUs can be monitored"
msgstr "GPU 支援處於實驗階段，只能監控 AMD 和 nVidia 的 GPU"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:45
msgid "Comments, suggestions, bug reports and contributions welcome"
msgstr "歡迎提供評論、建議、錯誤報告和貢獻"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:122
msgid ""
"Add an option, to the context menu, to show kernel times, in the CPU graphs"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:123
msgid "Add CPU temperature, where possible"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:124
msgid ""
"Implement smarter rounding and display precision of values throughout the "
"app by QwertyChouskie"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:125
msgid ""
"Use metainfo instead of appdata for application metadata by David Guglielmi"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:127
msgid "Fixes:"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:129
msgid ""
"Fixed a regression where natively installed browsers were not showing up in "
"the apps list on Fedora"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:130
msgid ""
"Fixed a regression where Flatpak apps were not showing up in the apps list "
"on ArchLinux Plasma"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:131
#, fuzzy
msgid ""
"Fixed a bug where the Maps app desktop file wasn't being parsed correctly"
msgstr "修復了一個錯誤，導致某些使用者的應用程式和處理程序列表為空"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:132
msgid ""
"Moved some performance related logs to g_debug to prevent spamming of system "
"logs"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:134
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:177
#, fuzzy
msgid "Translations:"
msgstr "虛擬化："

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:136
msgid "New translation to Korean by Seong-ho Cho"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:137
msgid "New translation to Dutch by Gert"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:138
msgid "New translation to Polish by _Ghost_"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:139
msgid "New translation to Italian by beppeilgommista"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:140
#, fuzzy
msgid "New translation to Portuguese (Brazil) by Gérson da Fonseca Henzel"
msgstr "由 Rafael Fontenelle 修正葡萄牙語翻譯"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:141
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:186
msgid "Updated Spanish translation"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:142
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:145
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:190
msgid "Updated Finnish translation"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:143
msgid "Updated Russian translation"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:144
msgid "Updated Chinese (Simplified) translation"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:146
msgid "Updated Greek translation"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:147
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:189
#, fuzzy
msgid "Updated German translation"
msgstr "由 ondra05 新增捷克語翻譯"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:155
msgid ""
"Hotfix release to fix a bug that caused the app to not display any "
"information for some users"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:164
msgid "New app icon by QwertyChouskie"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:165
msgid "Add ability to stop and force stop apps and processes"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:166
msgid ""
"Running apps are now shown more reliably, and should reflect most if not all "
"running apps"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:167
msgid "Experimental support for Snap apps in the running apps list"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:168
msgid ""
"Added a setting to show resource consumption individually per process or "
"cumulated with their descendants"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:171
msgid ""
"Added a setting to enable persistent sorting in the apps and processes list"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:172
msgid ""
"Data gathering is now more versatile and will permit new features to be "
"added quicker and easier"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:175
msgid "App can now be built from GNOME Builder"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:179
msgid "New translation to Norwegian Bokmål by Allan Nordhøy"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:180
msgid "New translation to Russian by Ivan Maslikhov"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:181
msgid "New translation to Slovak by mthw0"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:182
msgid "New translation to Greek by Yiannis Ioannides"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:183
msgid "New translation to Chinese (Simplified) by foxer NS"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:184
msgid "New translation to French by Link Mauve"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:185
msgid "New translation to Hungarian by Kovács Bálint Hunor"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:187
#, fuzzy
msgid "Updated Czech translation"
msgstr "由 ondra05 新增捷克語翻譯"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:188
#, fuzzy
msgid "Updated Portuguese translation"
msgstr "由 Rilson Joás 新增葡萄牙語翻譯"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:191
msgid "Fixes for Chinese translations by foxer NS"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:199
msgid "Add Spanish translation by Óscar Fernández Díaz"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:200
msgid ""
"The memory tab now shows configured memory speed instead of the maximum "
"supported by the modules"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:203
#, fuzzy
msgid "Add German translation by TecCheck"
msgstr "由 ondra05 新增捷克語翻譯"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:204
msgid ""
"When a process uses large amounts of CPU or RAM it is now highlighted in the "
"Apps and Processes list"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:207
msgid "Add initial support for building for ARM64"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:208
msgid ""
"Take into account multiple CPU cores and cache sharing when calculating "
"cache sizes"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:209
msgid ""
"Fix browsers, installed as native packages, not showing up in the Apps list"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:210
#, fuzzy
msgid "Translation updates for Traditional Chinese by Peter Dave Hello"
msgstr "由 Peter Dave Hello 新增繁體中文語系"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:218
msgid "Translation fixes for Portuguese by Rafael Fontenelle"
msgstr "由 Rafael Fontenelle 修正葡萄牙語翻譯"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:219
msgid ""
"Only show a link-local IPv6 address if no other IPv6 exists by Maximilian"
msgstr "由 Maximilian 提供，如果沒有其他 IPv6，則只顯示連結本地 IPv6 位址"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:220
msgid "Add Traditional Chinese locale by Peter Dave Hello"
msgstr "由 Peter Dave Hello 新增繁體中文語系"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:221
msgid "Add category for application menu by Renner0E"
msgstr "由 Renner0E 為應用程式選單新增類別"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:222
msgid ""
"Fix a parsing error when parsing the output of `dmidecode` that lead to a "
"panic"
msgstr "修復解析 `dmidecode` 輸出時導致的解析錯誤"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:223
msgid ""
"Use a fallback if `/sys/devices/system/cpu/cpu0/cpufreq/base_frequency` does "
"not exist, when getting CPU base speed information"
msgstr ""
"在取得 CPU 基礎速度資訊時，如果 `/sys/devices/system/cpu/cpu0/cpufreq/"
"base_frequency` 不存在，則使用回退"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:226
msgid "Update GPU tab UI to be more adaptive for smaller resolutions"
msgstr "更新 GPU 標籤 UI 以適應較小的解析度"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:234
msgid "Added Czech translation by ondra05"
msgstr "由 ondra05 新增捷克語翻譯"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:235
msgid "Added Portuguese translation by Rilson Joás"
msgstr "由 Rilson Joás 新增葡萄牙語翻譯"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:236
msgid ""
"Add keywords to desktop file to improve search function of desktop "
"environments by Hannes Kuchelmeister"
msgstr "由 Hannes Kuchelmeister 為桌面檔案新增關鍵字以改進桌面環境的搜尋功能"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:239
msgid "Fixed a bug where the app and process list was empty for some users"
msgstr "修復了一個錯誤，導致某些使用者的應用程式和處理程序列表為空"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:247
msgid "Fix a crash that occurs when the system is under heavy load"
msgstr "修復系統負載過重時發生的崩潰"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:255
msgid "First official release!"
msgstr "首次正式發布！"

#: data/io.missioncenter.MissionCenter.gschema.xml:29
msgid "Which page is shown on application startup"
msgstr "應用程式啟動時顯示的頁面"

#: data/io.missioncenter.MissionCenter.gschema.xml:35
msgid "How fast should the data be refreshed and the UI updated"
msgstr "資料和使用者介面更新的頻率為何"

#: data/io.missioncenter.MissionCenter.gschema.xml:40
#: resources/ui/preferences/page.blp:38
msgid "Parent and child process stats are shown individually or merged upwards"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:45
msgid "Column sorting is persisted across app restarts"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:50
msgid "The column id by which the Apps page view is sorted"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:55
msgid "The sorting direction of the Apps page view"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:61
msgid "Which graph is shown on the CPU performance page"
msgstr "在 CPU 效能頁面上顯示哪個圖表"

#: data/io.missioncenter.MissionCenter.gschema.xml:66
msgid "Show kernel times in the CPU graphs"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:71
msgid "Which page is shown on application startup, in the performance tab"
msgstr "在效能標籤中，應用程式啟動時顯示的頁面"

#: resources/ui/performance_page/cpu.blp:42 src/apps_page/mod.rs:1049
#: src/performance_page/mod.rs:264
msgid "CPU"
msgstr "CPU"

#: resources/ui/performance_page/cpu.blp:74
#: resources/ui/performance_page/cpu.blp:152
#: resources/ui/performance_page/gpu.blp:227
msgid "Utilization"
msgstr "使用率"

#: resources/ui/performance_page/cpu.blp:84
msgid "100%"
msgstr "100%"

#: resources/ui/performance_page/cpu.blp:174
msgid "Speed"
msgstr "速度"

#: resources/ui/performance_page/cpu.blp:201 src/apps_page/mod.rs:454
msgid "Processes"
msgstr "處理程序"

#: resources/ui/performance_page/cpu.blp:223
msgid "Threads"
msgstr "執行緒"

#: resources/ui/performance_page/cpu.blp:245
msgid "Handles"
msgstr "控制代碼"

#: resources/ui/performance_page/cpu.blp:268
msgid "Up time"
msgstr "運作時間"

#: resources/ui/performance_page/cpu.blp:294
msgid "Base Speed:"
msgstr "基本速度："

#: resources/ui/performance_page/cpu.blp:303
msgid "Sockets:"
msgstr "插槽："

#: resources/ui/performance_page/cpu.blp:312
msgid "Virtual processors:"
msgstr "虛擬處理器："

#: resources/ui/performance_page/cpu.blp:321
msgid "Virtualization:"
msgstr "虛擬化："

#: resources/ui/performance_page/cpu.blp:330
msgid "Virtual machine:"
msgstr "虛擬機器："

#: resources/ui/performance_page/cpu.blp:339
msgid "L1 cache:"
msgstr "L1 快取："

#: resources/ui/performance_page/cpu.blp:348
msgid "L2 cache:"
msgstr "L2 快取："

#: resources/ui/performance_page/cpu.blp:357
msgid "L3 cache:"
msgstr "L3 快取："

#: resources/ui/performance_page/cpu.blp:442
msgid "Change G_raph To"
msgstr "更改圖表為"

#: resources/ui/performance_page/cpu.blp:445
msgid "Overall U_tilization"
msgstr "整體使用率"

#: resources/ui/performance_page/cpu.blp:450
msgid "Logical _Processors"
msgstr "邏輯處理器"

#: resources/ui/performance_page/cpu.blp:456
msgid "Show Kernel Times"
msgstr ""

#: resources/ui/performance_page/cpu.blp:463
#: resources/ui/performance_page/disk.blp:386
#: resources/ui/performance_page/gpu.blp:549
#: resources/ui/performance_page/memory.blp:402
#: resources/ui/performance_page/network.blp:369
msgid "Graph _Summary View"
msgstr "圖表總覽"

#: resources/ui/performance_page/cpu.blp:468
#: resources/ui/performance_page/disk.blp:391
#: resources/ui/performance_page/gpu.blp:554
#: resources/ui/performance_page/memory.blp:407
#: resources/ui/performance_page/network.blp:374
msgid "_View"
msgstr "檢視"

#: resources/ui/performance_page/cpu.blp:471
#: resources/ui/performance_page/disk.blp:394
#: resources/ui/performance_page/gpu.blp:557
#: resources/ui/performance_page/memory.blp:410
#: resources/ui/performance_page/network.blp:377
msgid "CP_U"
msgstr "CPU"

#: resources/ui/performance_page/cpu.blp:476
#: resources/ui/performance_page/disk.blp:399
#: resources/ui/performance_page/gpu.blp:562
#: resources/ui/performance_page/memory.blp:415
#: resources/ui/performance_page/network.blp:382
msgid "_Memory"
msgstr "記憶體"

#: resources/ui/performance_page/cpu.blp:481
#: resources/ui/performance_page/disk.blp:404
#: resources/ui/performance_page/gpu.blp:567
#: resources/ui/performance_page/memory.blp:420
#: resources/ui/performance_page/network.blp:387
msgid "_Disk"
msgstr "磁碟"

#: resources/ui/performance_page/cpu.blp:486
#: resources/ui/performance_page/disk.blp:409
#: resources/ui/performance_page/gpu.blp:572
#: resources/ui/performance_page/memory.blp:425
#: resources/ui/performance_page/network.blp:392
msgid "_Network"
msgstr "網路"

#: resources/ui/performance_page/cpu.blp:491
#: resources/ui/performance_page/disk.blp:414
#: resources/ui/performance_page/gpu.blp:577
#: resources/ui/performance_page/memory.blp:430
#: resources/ui/performance_page/network.blp:397
msgid "_GPU"
msgstr "GPU"

#: resources/ui/performance_page/cpu.blp:499
#: resources/ui/performance_page/disk.blp:422
#: resources/ui/performance_page/gpu.blp:585
#: resources/ui/performance_page/memory.blp:438
#: resources/ui/performance_page/network.blp:410
msgid "_Copy"
msgstr "複製"

#: resources/ui/performance_page/disk.blp:64
#: resources/ui/performance_page/disk.blp:188
msgid "Active time"
msgstr "活動時間"

#: resources/ui/performance_page/disk.blp:122
msgid "Disk transfer rate"
msgstr "磁碟傳輸速率"

#: resources/ui/performance_page/disk.blp:211
msgid "Average response time"
msgstr "平均回應時間"

#: resources/ui/performance_page/disk.blp:247
msgid "Read speed"
msgstr "讀取速度"

#: resources/ui/performance_page/disk.blp:279
msgid "Write speed"
msgstr "寫入速度"

#: resources/ui/performance_page/disk.blp:307
msgid "Capacity:"
msgstr "容量："

#: resources/ui/performance_page/disk.blp:316
msgid "Formatted:"
msgstr "已格式化："

#: resources/ui/performance_page/disk.blp:325
msgid "System disk:"
msgstr "系統磁碟："

#: resources/ui/performance_page/disk.blp:334
#: resources/ui/performance_page/memory.blp:345
msgid "Type:"
msgstr "類型："

#: resources/ui/performance_page/gpu.blp:71
msgid "Overall utilization"
msgstr "整體使用率"

#: resources/ui/performance_page/gpu.blp:110
msgid "Video encode"
msgstr "視訊編碼"

#: resources/ui/performance_page/gpu.blp:143
msgid "Video decode"
msgstr "視訊解碼"

#: resources/ui/performance_page/gpu.blp:180
#: resources/ui/performance_page/gpu.blp:350
#: resources/ui/performance_page/memory.blp:73
msgid "Memory usage"
msgstr "記憶體使用率"

#: resources/ui/performance_page/gpu.blp:250
msgid "Clock Speed"
msgstr "時脈速度"

#: resources/ui/performance_page/gpu.blp:295
msgid "Power draw"
msgstr "功率消耗"

#: resources/ui/performance_page/gpu.blp:395
msgid "Memory speed"
msgstr "記憶體速度"

#: resources/ui/performance_page/gpu.blp:440
msgid "Temperature"
msgstr "溫度"

#: resources/ui/performance_page/gpu.blp:470
msgid "OpenGL version:"
msgstr "OpenGL 版本："

#: resources/ui/performance_page/gpu.blp:479
msgid "Vulkan version:"
msgstr "Vulkan 版本："

#: resources/ui/performance_page/gpu.blp:488
msgid "PCI Express speed:"
msgstr "PCI Express 速度："

#: resources/ui/performance_page/gpu.blp:497
msgid "PCI bus address:"
msgstr "PCI 匯流排位址："

#: resources/ui/performance_page/memory.blp:28
msgid "Some information requires administrative privileges"
msgstr "某些資訊需要管理員權限"

#: resources/ui/performance_page/memory.blp:29
msgid "_Authenticate"
msgstr "驗證"

#: resources/ui/performance_page/memory.blp:52 src/apps_page/mod.rs:1055
#: src/performance_page/mod.rs:319
msgid "Memory"
msgstr "記憶體"

#: resources/ui/performance_page/memory.blp:130
msgid "Memory composition"
msgstr "記憶體組成"

#: resources/ui/performance_page/memory.blp:166
msgid "In use"
msgstr "使用中"

#: resources/ui/performance_page/memory.blp:189
msgid "Available"
msgstr "可用"

#: resources/ui/performance_page/memory.blp:217
msgid "Committed"
msgstr "已提交"

#: resources/ui/performance_page/memory.blp:240
msgid "Cached"
msgstr "已快取"

#: resources/ui/performance_page/memory.blp:267
msgid "Swap available"
msgstr "可用交換空間"

#: resources/ui/performance_page/memory.blp:290
msgid "Swap used"
msgstr "已使用交換空間"

#: resources/ui/performance_page/memory.blp:318
msgid "Speed:"
msgstr "速度："

#: resources/ui/performance_page/memory.blp:327
msgid "Slots used:"
msgstr "已使用插槽："

#: resources/ui/performance_page/memory.blp:336
msgid "Form factor:"
msgstr "尺寸規格："

#: resources/ui/performance_page/network.blp:62
msgid "Throughput"
msgstr "吞吐量"

#: resources/ui/performance_page/network.blp:139
msgid "Send"
msgstr "傳送"

#: resources/ui/performance_page/network.blp:170
msgid "Receive"
msgstr "接收"

#: resources/ui/performance_page/network.blp:197
msgid "Interface name:"
msgstr "介面名稱："

#: resources/ui/performance_page/network.blp:206
msgid "Connection type:"
msgstr "連線類型："

#: resources/ui/performance_page/network.blp:216
msgid "SSID:"
msgstr "SSID："

#: resources/ui/performance_page/network.blp:226
msgid "Signal strength:"
msgstr "訊號強度："

#: resources/ui/performance_page/network.blp:236
msgid "Maximum Bitrate:"
msgstr "最大傳輸速率："

#: resources/ui/performance_page/network.blp:246
msgid "Frequency:"
msgstr "頻率："

#: resources/ui/performance_page/network.blp:255
msgid "Hardware address:"
msgstr "硬體位址："

#: resources/ui/performance_page/network.blp:264
msgid "IPv4 address:"
msgstr "IPv4 位址："

#: resources/ui/performance_page/network.blp:273
msgid "IPv6 address:"
msgstr "IPv6 位址："

#: resources/ui/performance_page/network.blp:403
msgid "Network Se_ttings"
msgstr "網路設定"

#: resources/ui/preferences/page.blp:6
msgid "General Settings"
msgstr "一般設定"

#: resources/ui/preferences/page.blp:9
msgid "Update Speed"
msgstr "更新速度"

#: resources/ui/preferences/page.blp:12 src/preferences/page.rs:113
#: src/preferences/page.rs:262
msgid "Fast"
msgstr "快"

#: resources/ui/preferences/page.blp:13
msgid "Refresh every half second"
msgstr "每半秒更新一次"

#: resources/ui/preferences/page.blp:17 src/preferences/page.rs:109
#: src/preferences/page.rs:122 src/preferences/page.rs:266
#: src/preferences/page.rs:282
msgid "Normal"
msgstr "正常"

#: resources/ui/preferences/page.blp:18
msgid "Refresh every second"
msgstr "每秒更新一次"

#: resources/ui/preferences/page.blp:22 src/preferences/page.rs:105
#: src/preferences/page.rs:270
msgid "Slow"
msgstr "慢"

#: resources/ui/preferences/page.blp:23
msgid "Refresh every second and a half"
msgstr "每 1.5 秒更新一次"

#: resources/ui/preferences/page.blp:27 src/preferences/page.rs:101
#: src/preferences/page.rs:274
msgid "Very Slow"
msgstr "非常慢"

#: resources/ui/preferences/page.blp:28
msgid "Refresh every 2 seconds"
msgstr "每 2 秒更新一次"

#: resources/ui/preferences/page.blp:34
#, fuzzy
msgid "App Page Settings"
msgstr "一般設定"

#: resources/ui/preferences/page.blp:37
msgid "Merge Process Stats"
msgstr ""

#: resources/ui/preferences/page.blp:42
msgid "Remember Sorting"
msgstr ""

#: resources/ui/preferences/page.blp:43
#, fuzzy
msgid "Remember the sorting of the app and process list across app restarts"
msgstr "修復了一個錯誤，導致某些使用者的應用程式和處理程序列表為空"

#: resources/ui/window.blp:48
msgid "Type a name or PID to search"
msgstr "輸入名稱或 PID 進行搜尋"

#: resources/ui/window.blp:90
msgid "Loading..."
msgstr ""

#: resources/ui/window.blp:104
msgid "Performance"
msgstr "效能"

#: resources/ui/window.blp:113 src/apps_page/mod.rs:447
msgid "Apps"
msgstr "應用程式"

#: resources/ui/window.blp:124
msgid "_Preferences"
msgstr "偏好設定"

#: resources/ui/window.blp:129
msgid "_About MissionCenter"
msgstr "關於任務中心"

#: src/apps_page/mod.rs:1037
msgid "Name"
msgstr "名稱"

#: src/apps_page/mod.rs:1043
msgid "PID"
msgstr "PID"

#: src/apps_page/mod.rs:1061
msgid "Disk"
msgstr "磁碟"

#. ContentType::App
#: src/apps_page/list_item.rs:315
msgid "Stop Application"
msgstr ""

#: src/apps_page/list_item.rs:315
msgid "Force Stop Application"
msgstr ""

#. ContentType::Process
#: src/apps_page/list_item.rs:319
#, fuzzy
msgid "Stop Process"
msgstr "處理程序"

#: src/apps_page/list_item.rs:319
msgid "Force Stop Process"
msgstr ""

#: src/performance_page/widgets/mem_composition_widget.rs:224
msgid ""
"In use ({}B)\n"
"\n"
"Memory used by the operating system and running applications"
msgstr ""
"使用中（{}B）\n"
"\n"
"作業系統和正在值行的應用程式使用的記憶體"

#: src/performance_page/widgets/mem_composition_widget.rs:236
msgid ""
"Modified ({}B)\n"
"\n"
"Memory whose contents must be written to disk before it can be used by "
"another process"
msgstr ""
"已修改（{}B）\n"
"\n"
"在其他處理程序可以使用之前，必須先將記憶體內容寫入磁碟"

#: src/performance_page/widgets/mem_composition_widget.rs:257
msgid ""
"Standby ({}B)\n"
"\n"
"Memory that contains cached data and code that is not actively in use"
msgstr ""
"待機（{}B）\n"
"\n"
"包含非活動使用的快取資料和程式碼的記憶體"

#: src/performance_page/widgets/mem_composition_widget.rs:266
msgid ""
"Free ({}B)\n"
"\n"
"Memory that is not currently in use, and that will be repurposed first when "
"the operating system, drivers, or applications need more memory"
msgstr ""
"空閒（{}B）\n"
"\n"
"當作業系統、驅動程式或應用程式需要更多記憶體時，優先重新分配的未使用的記憶體"

#: src/performance_page/mod.rs:372 src/performance_page/disk.rs:195
msgid "Disk {} ({})"
msgstr "磁碟 {} ({})"

#: src/performance_page/mod.rs:376
msgid "HDD"
msgstr "HDD"

#: src/performance_page/mod.rs:377
msgid "SSD"
msgstr "SSD"

#: src/performance_page/mod.rs:378
msgid "NVMe"
msgstr "NVMe"

#: src/performance_page/mod.rs:379
msgid "eMMC"
msgstr "eMMC"

#: src/performance_page/mod.rs:380
msgid "iSCSI"
msgstr "iSCSI"

#: src/performance_page/mod.rs:381 src/performance_page/cpu.rs:310
#: src/performance_page/cpu.rs:323 src/performance_page/cpu.rs:333
#: src/performance_page/cpu.rs:339 src/performance_page/gpu.rs:255
#: src/performance_page/memory.rs:338 src/performance_page/network.rs:375
#: src/performance_page/network.rs:396 src/performance_page/network.rs:405
#: src/performance_page/network.rs:442 src/performance_page/network.rs:520
msgid "Unknown"
msgstr "未知"

#: src/performance_page/mod.rs:451 src/performance_page/network.rs:331
msgid "Ethernet"
msgstr "乙太網路"

#: src/performance_page/mod.rs:452 src/performance_page/network.rs:338
msgid "Wi-Fi"
msgstr "Wi-Fi"

#: src/performance_page/mod.rs:453 src/performance_page/network.rs:340
msgid "Other"
msgstr "其他"

#: src/performance_page/mod.rs:541
msgid "GPU {}"
msgstr "GPU {}"

#: src/performance_page/mod.rs:702 src/performance_page/mod.rs:710
#, fuzzy
msgid "{}: {} {}bps"
msgstr "{} {}bps"

#: src/performance_page/cpu.rs:318
msgid "Supported"
msgstr "支援"

#: src/performance_page/cpu.rs:320 src/performance_page/gpu.rs:266
msgid "Unsupported"
msgstr "不支援"

#: src/performance_page/cpu.rs:328 src/performance_page/disk.rs:223
msgid "Yes"
msgstr "是"

#: src/performance_page/cpu.rs:330 src/performance_page/disk.rs:225
msgid "No"
msgstr "否"

#: src/performance_page/cpu.rs:698
#, fuzzy
msgid "Utilization over {} seconds"
msgstr "{} 秒內的 % 使用率"

#: src/performance_page/cpu.rs:702 src/performance_page/disk.rs:380
#: src/performance_page/memory.rs:401 src/performance_page/network.rs:606
msgid "{} seconds"
msgstr "{} 秒"

#: src/performance_page/disk.rs:251
msgid "{} {}{}B/s"
msgstr "{} {}{}B/s"

#: src/performance_page/network.rs:416 src/performance_page/network.rs:422
#: src/performance_page/network.rs:430
msgid "{} {}bps"
msgstr "{} {}bps"

#: src/performance_page/network.rs:455 src/performance_page/network.rs:471
msgid "N/A"
msgstr "無法取得"

#: src/application.rs:261
msgid "translator-credits"
msgstr "Peter Dave Hello <hsu@peterdavehello.org>"

#~ msgid ""
#~ "The application currently only supports monitoring, you cannot stop "
#~ "processes for example"
#~ msgstr "該應用程式目前只支援監控功能，舉例來說您無法停止處理程序"

#~ msgid "% Utilization"
#~ msgstr "% 使用率"

#~ msgid "{}: {} {}bps {}: {} {}bps"
#~ msgstr "{}: {} {}bps {}: {} {}bps"

#~ msgid "_Quit"
#~ msgstr "退出"
